ZinatFilters
=================

Instagram-like image filters. Project based on [@acoomans](https://github.com/acoomans/instagram-filters/) repo.

## Dependencies

Instagram-filters depends on ImageMagick, which can be installed on a Mac with brew:

	brew install imagemagick

## Install

	python setup.py install


## Usage

First, import the client:

	from instagram_filters.filters import *

Instanciate a filter and apply it:

	f = Nashville("image.jpg")
	f.apply()

Available filters: 

- Gotham
- Kelvin
- Lomo
- Nashville
- Toaster

**Note** The filters change the image in-place. Be sure to copy it before applying any filter if you want to copy the original image.


# Tests

Run the tests with:

	cd tests
	python test.py
	
## Credits

This library is inspired from the ["Create instagram filters with php" tutsplus tutorial](http://net.tutsplus.com/tutorials/php/create-instagram-filters-with-php/).
